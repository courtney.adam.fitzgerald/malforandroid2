package com.github.riotopsys.malforandroid2.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.github.riotopsys.malforandroid2.R;

public class DonateActivity extends BaseActivity implements OnClickListener {

	private static final String TAG = DonateActivity.class.getSimpleName();

	private IInAppBillingService mService;

	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.donate);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);

		bindService(new Intent(
				"com.android.vending.billing.InAppBillingService.BIND"),
				mServiceConn, Context.BIND_AUTO_CREATE);

		findViewById(R.id.buy_test_item).setOnClickListener(this);
		findViewById(R.id.small_donation).setOnClickListener(this);
		findViewById(R.id.medium_donation).setOnClickListener(this);
		findViewById(R.id.large_donation).setOnClickListener(this);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mServiceConn != null) {
			unbindService(mServiceConn);
		}
	}

	@Override
	public void onClick(View v) {
		int id = R.string.iap_test;
		switch (v.getId()) {
		case R.id.buy_test_item:
			id = R.string.iap_test;
			break;
		case R.id.small_donation:
			id = R.string.iap_small;
			break;
		case R.id.medium_donation:
			id = R.string.iap_medium;
			break;
		case R.id.large_donation:
			id = R.string.iap_grande;
			break;
		default:
		}
		makeDonation(id);
	}

	private void makeDonation(int id) {
		try {
			Bundle buyIntentBundle = mService.getBuyIntent(3, getPackageName(),
					   getString(id), "inapp", "DEAFBEEF");
			
			PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
			
			try {
				startIntentSenderForResult(pendingIntent.getIntentSender(),
						   1001, new Intent(), 0, 0, 0);
			} catch (SendIntentException e) {
				Log.e(TAG, "", e);
			}
		} catch (RemoteException e) {
			Log.e(TAG, "", e);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) { 
	   if (requestCode == 1001) {           
//	      int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
	      String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
//	      String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
	        
	      if (resultCode == RESULT_OK) {
	         try {
	            JSONObject jo = new JSONObject(purchaseData);
//	            String sku = jo.getString("productId");
	            String token = jo.getString("purchaseToken");
	            alert(getString(R.string.donate_thanks));
	            
	            new AsyncTask<String, Void, Void>(){

					@Override
					protected Void doInBackground(String... tokens) {
						try {
							for ( String token: tokens){
								mService.consumePurchase(3, getPackageName(), token);
							}
						} catch (RemoteException e) {
							Log.e(TAG, "", e);
						}
						return null;
					}
	            	
	            }.execute(token);
	            
	          }
	          catch (JSONException e) {
	             alert("Failed to parse purchase data.");
	             e.printStackTrace();
	          }
	      }
	   }
	}

	private void alert(String string) {
		Toast.makeText(this, string, Toast.LENGTH_LONG).show();
	}

}
